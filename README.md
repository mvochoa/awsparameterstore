# AWS Parameter Store

Kustomize plugin to AWS Systems Manager Parameter Store

Download binary [release](https://gitlab.com/mvochoa/awsparameterstore/-/releases) 

## Install Plugin

Run commands:

```sh
$ export VERSION=v1.0.1
$ export PLUGIN_DIR=~/.config/kustomize/plugin/mvochoa.gitlab.com/$VERSION/secretgenerationparameterstore
$ mkdir -p $PLUGIN_DIR
$ curl -L -o $PLUGIN_DIR/SecretGenerationParameterStore https://gitlab.com/mvochoa/awsparameterstore/-/releases/$VERSION/downloads/binaries/aws_parameter_store_linux_amd64
$ chmod +x $PLUGIN_DIR/SecretGenerationParameterStore
```

## Example file


```yaml
# customGenerator.yaml
apiVersion: mvochoa.gitlab.com/v1.0.1
kind: SecretGenerationParameterStore
metadata:
  name: my-secret
behavior: merge
spec:
  regionAWS: us-west-2
  data:
    - key: /staging/service/test
      name: password
```

```yaml
# kustomization.yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

generators:
  - customGenerator.yaml
```

Run command `AWS_PROFILE=[profile] kustomize build --enable-alpha-plugins .` result:

```yaml
apiVersion: v1
type: Opaque
metadata:
  name: mySecret-dd8554k82c
data:
  password: SGVsbG8gV29ybGQ=
kind: Secret
```
