package awsparameterstore

import (
	"fmt"

	"sigs.k8s.io/kustomize/api/kv"
	"sigs.k8s.io/kustomize/api/provider"
	"sigs.k8s.io/kustomize/api/resmap"
	"sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/yaml"
)

type Data struct {
	Key  string `json:"key" yaml:"key"`
	Name string `json:"name" yaml:"name"`
}

type Spec struct {
	RegionAws string `json:"regionAws" yaml:"regionAws"`
	Data      []Data `json:"data" yaml:"data" `
}

type Plugin struct {
	h                *resmap.PluginHelpers
	types.ObjectMeta `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Behavior         string `json:"behavior,omitempty" yaml:"behavior,omitempty"`
	Spec             `json:"spec" yaml:"spec"`
}

func (p *Plugin) Config(config []byte) (err error) {
	provider := provider.NewDefaultDepProvider()
	resmapFactory := resmap.NewFactory(provider.GetResourceFactory())
	p.h = resmap.NewPluginHelpers(nil, provider.GetFieldValidator(), resmapFactory, nil)

	if p.Annotations == nil {
		p.Annotations = make(map[string]string)
	}

	return yaml.Unmarshal(config, p)
}

func (p *Plugin) Generate() []byte {
	args := types.SecretArgs{}
	args.Name = p.Name
	args.Namespace = p.Namespace
	if p.Behavior != "" {
		args.Behavior = p.Behavior
		p.Annotations["kustomize.config.k8s.io/behavior"] = p.Behavior
	}

	args.Options = &types.GeneratorOptions{
		Annotations: p.Annotations,
	}

	sessionSMM := NewSessionSSM(p.RegionAws)
	for _, d := range p.Spec.Data {
		value := fmt.Sprintf("%v=%v", d.Name, GetParameter(sessionSMM, d.Key))
		args.LiteralSources = append(args.LiteralSources, value)
	}

	secret, _ := p.h.ResmapFactory().FromSecretArgs(
		kv.NewLoader(p.h.Loader(), p.h.Validator()), args)
	result, _ := secret.AsYaml()
	return result
}
