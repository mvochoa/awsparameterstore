package awsparameterstore

import (
	"fmt"
	"log"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ssm"
)

func NewSessionSSM(region string) *ssm.SSM {
	session, err := session.NewSessionWithOptions(session.Options{
		Config: aws.Config{
			Region: aws.String("us-west-1"),
		},
		SharedConfigState: session.SharedConfigEnable,
	})
	if err != nil {
		log.Panic(err)
	}

	awsConfig := aws.NewConfig()
	if region != "" {
		awsConfig.WithRegion(region)
	}

	return ssm.New(session, awsConfig)
}

func GetParameter(sessionSSM *ssm.SSM, name string) string {
	param, err := sessionSSM.GetParameter(&ssm.GetParameterInput{
		Name:           aws.String(name),
		WithDecryption: aws.Bool(false),
	})
	if err != nil {
		if err.Error() == "ParameterNotFound: " {
			fmt.Fprintf(os.Stderr, "Error: ParameterNotFound: %s\n", name)
			os.Exit(1)
		}
		log.Panic(err)
	}

	return *param.Parameter.Value
}
